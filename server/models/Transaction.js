//this would serve as the model
const mongoose = require("mongoose");
const Schema = mongoose.Schema; //calls Schema for us to be able to create schemas

const TransactionSchema = new Schema(
	{
		

		bookDate: {
			type: String,
			required: true
		},

		memberId: {
			type: String,
			required: true

		},

		availedNumber: {
			type: Number,
			required: true
		},

	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Transaction", TransactionSchema);