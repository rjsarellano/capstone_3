//this would serve as the model
const mongoose = require("mongoose");
const Schema = mongoose.Schema; //calls Schema for us to be able to create schemas

const RoomSchema = new Schema(
	{
		description: {
			type: String,
			required: true
		},

		status: {
			type: String,
			required: true
		},


		bookedByMemberId: {
			type: String,
			required: true
		},


	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Room", RoomSchema);