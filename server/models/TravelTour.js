//this would serve as the model
const mongoose = require("mongoose");
const Schema = mongoose.Schema; //calls Schema for us to be able to create schemas

const TravelTourSchema = new Schema(
	{
		description: {
			type: String,
			required: true
		},

		price: {
			type: String,
			required: true
		},


		availability: {
			type: Number,
			required: true
		},

	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("TravelTours", TravelTourSchema);