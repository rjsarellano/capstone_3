//this would serve as the model
const mongoose = require("mongoose");
const Schema = mongoose.Schema; //calls Schema for us to be able to create schemas

const MemberSchema = new Schema(
	{
		firstName: {
			type: String,
			required: true
		},

		lastName: {
			type: String,
			required: true
		},

		email: {
			type:String,
			required:true
		},

		password: {
			type:String,
			required:true
		},


		country: String,
		
		contact: Number,


	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Member", MemberSchema);