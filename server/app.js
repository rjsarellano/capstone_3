//Backend server
//declare and use the dependencies

const express = require("express"); //use the express library
const app = express(); //app is now our server
const mongoose = require("mongoose");
const cors = require("cors");

//Databse connection
/*mongoose.connect("mongodb://localhost:27017/b33_merng_tracker", {
	useNewUrlParser: true
});*/

const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://rsarellano:5Da7lgkooc123$@cluster0-3h6es.mongodb.net/Capstone_3?retryWrites=true&w=majority";

mongoose.connect( databaseUrl,
	{
		useNewUrlParser: true
	}
);

mongoose.connection.once("open", () => {
	console.log("Now connected to the online MongoDB server");
});

//import the instatiation of the
const server = require("./queries/queries.js");

app.use(cors());

//the app will be served by apollo server instead of express
server.applyMiddleware({ app });

//server initialization
const port = process.env.PORT || 4000;
app.listen(port, () => {
	//console.log("Now listening for requests on port " + port);
	console.log(
		`🚀  Server ready at port ${port}${server.graphqlPath}`
	);
});
