const { ApolloServer, gql } = require("apollo-server-express"); //import apollo-server-express
const { GraphQLDateTime } = require("graphql-iso-date"); //import date data type
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");


// mongoose models
const Member = require("../models/Member");
const TravelTours = require("../models/TravelTour");
const Transaction = require("../models/Transaction");



const customScalarResolver = {
	Date: GraphQLDateTime
};

//important components
const typeDefs = gql`
	scalar Date

	#customized Query
	


	type TravelTourType {
		id: ID!
		description: String!
		price: String!
		availability: Int!
		createdAt: Date
		updatedAt: Date
	}


	type MemberType {
		id: ID!
		firstName: String!
		lastName: String!
		email: String!
		contact: String!
		password: String!
		country:String!
		token: String!
		createdAt: Date
		updatedAt: Date
		
	}


	type TransactionType {
		id: ID!
		memberId: String!
		member: [MemberType]
		bookDate: String!
		availedNumber: Int!


	}

	# this is a comment
	# the Query type is the root of all GraphQL queries
	# this is used for "GET"

	#R Retrieve
	#Query
	type Query {
		#create a query called hello that will expect a string data type
		hello: Boolean

		#square brackets indicate that we are expecting an array
		
		

		getTravelTours: [TravelTourType]

		getMember(id: String): [MemberType]

		getMemberByEmail (email: String): [MemberType]

		getTransactions(id: String): [TransactionType]

		getTransaction (memberId:String): [TransactionType]

		getTravelTour (id: String): [TravelTourType]

	}

	#CUD Create, Update, Delete
	#we are mutating the server/database
	#Mutation
	type Mutation {
	


		createMember(
			firstName: String!
			lastName: String!
			country: String!
			password: String!
			email: String!
			contact: String!
		): MemberType


		createTravelTour(
			description: String!
			price: String!
			availability: Int!
		): TravelTourType

		createTransaction(
			memberId:String!
			bookDate: String!
			availedNumber: Int!
		): TransactionType


		logInMember (
		id:String!
		email: String!
		password:String!
		token:String!
		firstName:String!
		lastName:String!
		): MemberType

		updateMember(
			id: String!
			firstName: String!
			lastName: String!
			position: String!
			teamId: String!
		): MemberType

		updateTour(
			id: String!
			description: String!
			price: String!
			availability: Int!
		): TravelTourType

			updateTransaction(
			id: String!
			availedNumber: Int!
		): TransactionType


		deleteMember(id: String!): Boolean

		deleteTransaction(id: String!): Boolean

		deleteTravelTour(id: String!): Boolean
	}
`;

//resolvers have 4 built-in parameters passed into them
//mutate: (parent, args, content, info)
const resolvers = {
	//Query
	Query: {
		// what are we going to return when the hello query is called
		// hello: () => "my first query"
		// hello: ["HI"]
		// hello: () => 5 + 10
		// hello: () => true

		

		getMemberByEmail(parent, args){
			if (!args.email){
				return Member.find({})
			} else {
				return Member.find({email: args.email})
			}
		},

		getTravelTours(parent, args) {
			if (!args.id) {
				return TravelTours.find({});
			} else {
				return TravelTours.find({ _id: args.id });
			}
		},

		getTravelTour(parent,args) {
			return TravelTours.find({_id : args.id})
		},

			getTransactions(parent, args) {
			if (!args.id) {
				return Transaction.find({});
			} else {
				return Transaction.find({_id: args.id });
			}
		},



			getTransaction(parent, args) {
			
				return Transaction.find({ memberId: args.memberId });
			
		},
		
		


		getMember(_, args) {
			console.log("fetching a single member")
			console.log(args);
			return Member.find({ _id: args.id });


		}
	},

	//customized Resolvers
	

	
	TransactionType: {
		member(parent, args) {
			console.log(parent);
			return Member.find({ _id: parent.memberId});
		}
	},


	
	//Mutation
	Mutation: {
		
		

		createMember: (_, args) => {
			// console.log(args)
			let newMember = Member({
				firstName: args.firstName,
				lastName: args.lastName,
				email: args.email,
				contact: args.contact,
				country: args.country,
				password: bcrypt.hashSync(args.password, 10),
				
			});

			// console.log(newMember)
			return newMember.save();
		},


		createTransaction: (_, args) => {
			// console.log(args)
			let NewTransaction = Transaction({
				memberId: args.memberId,
				bookDate: args.bookDate,
				availedNumber: args.availedNumber,

			});

			// console.log(newMember)
			return NewTransaction.save();
		},

		createTravelTour: (_, args) => {
			// console.log(args)
			let newTour = TravelTours({
				description: args.description,
				price: args.price,
				availability: args.availability,
			});

			// console.log(newMember)
			return newTour.save();
		},

		logInMember : (_, args) => {
				console.log(args)
				
				return Member.findOne({email: args.email}).then(member =>{
					console.log(member);
					
					if(member === null){
						return null;
					}
					let hashedPassword = bcrypt.compareSync(args.password, member.password)
					
					if(!hashedPassword){
						console.log("wrong password");
					} else {
						console.log(member)
						
						member.token = auth(member)
						console.log(member.token);
						return member;
					}
				})
			},

		updateMember: (_, args) => {
			// console.log(args)
			// let member = Member.find({_id: args.id})

			return Member.findOneAndUpdate(
				{ _id: args.id },
				{
					firstName: args.firstName,
					lastName: args.lastName,
					position: args.position,
					teamId: args.teamId
				}
			);
		},

		updateTransaction:(_, args) => {
			console.log(args)

			return Transaction.findOneAndUpdate(
					{ _id: args.id },
					{
						$set: {
						
						availedNumber: args.availedNumber

					}
					}
				);
		},

		updateTour: (_, args) => {
			console.log(args)
			return TravelTours.findOneAndUpdate(
				{ _id: args.id },
				{
					$set: {
						description: args.description,
						price: args.price,
						availability: args.availability
					}
				}
			);
		},


		deleteMember: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };

			return Member.deleteOne(condition).then(member => {
				console.log(member);
				if (member.deletedCount < 1) {
					console.log("nothing to delete");
					return false;
				}

				return true;
			});
		},


		deleteTransaction : (_, args) => {
			console.log(args);
			let condition = { _id: args.id };

			return Transaction.deleteOne(condition).then(Transaction => {
				console.log(Transaction);
				if (Transaction.deletedCount < 1) {
					console.log("nothing to delete");
					return false;
				}

				return true;
			});
		},
		

		deleteTravelTour : (_, args) => {
			console.log(args);
			let condition = { _id: args.id };

			return TravelTours.deleteOne(condition).then(TravelTours => {
				console.log(TravelTours);
				if (TravelTours.deletedCount < 1) {
					console.log("nothing to delete");
					return false;
				}

				return true;
			});
		},
		
		
	}
};

//create an instance of the Apollo Server
// in the most basic sense, the ApolloServer can be started by passing type definitions (typeDefs) and the resolvers responsible for fetching for those types

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
