import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Route, Switch,Redirect } from "react-router-dom";
import {
  Hero,
  Container,
  Heading,
  Columns,
  Login
} from "react-bulma-components";
import "react-bulma-components/dist/react-bulma-components.min.css";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

//components
import LoginPage from "./components/LoginPage.js";
import RegistrationPage from "./components/RegistrationPage.js";
import HeroHead from "./components/HeroHead.js";
import NavBar from "./components/NavBar.js";
import HomePage from "./components/HomePage.js";
import LogoutPage from "./components/LogoutPage.js";
import AdminPage from "./components/AdminPage.js";
import CommandPage from "./components/CommandPage.js";
import UpdateTransaction from "./components/UpdateTransaction.js";

const client = new ApolloClient({
  uri: "https://sleepy-earth-11720.herokuapp.com/graphql"
});

function App() {
  const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [email, setEmail] = useState(localStorage.getItem("email"));

  const updateSession = () => {
    setFirstName(localStorage.getItem("firstName"));
    setToken(localStorage.getItem("token"));
  };
  const loggedUser = props => (
    <Login {...props} updateSession={updateSession} />
  );

  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavBar email={email} firstname={firstName} />
        <Switch>
          <Route path="/HomePage" component={HomePage} />
          <Route exact path="/" render={() => (
    <Redirect to="/HomePage"/>
)}/>
          <Route path="/LoginPage" component={LoginPage} />
          <Route path="/AdminPage" component={AdminPage} />
          <Route path="/RegistrationPage" component={RegistrationPage} />
          <Route path="/logoutPage" component={LogoutPage} />
          <Route path="CommandPage" component={CommandPage} />
          <Route path="/HomePage" render={loggedUser} />
          <Route path="/UpdateTransaction" component={UpdateTransaction} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
