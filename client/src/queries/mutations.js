
import { gql } from "apollo-boost";

const createMemberMutation = gql`
	mutation(
			$firstName: String!
			$lastName: String!
			$country: String!
			$password: String!
			$email: String!
			$contact: String!
	) {
		createMember(
			firstName: $firstName
			lastName: $lastName
			country: $country
			password: $password
			email : $email
			contact: $contact
		) {
			firstName
			lastName
			country
			email
			contact

		}
	}
`;

const loginMemberMutation = gql`
  mutation(
 	 $id: String!
    $email: String!
    $password: String!
    $token: String!
    $firstName: String!
    $lastName: String!
  ){
    logInMember(
    	id: $id
      email: $email
      password: $password
      token: $token
      firstName: $firstName
      lastName: $lastName

    ){
    	id
      password
      email
      token
      firstName
      lastName
    }
  }
`;





const createTransactionMutation = gql`
		mutation(
			$memberId:String!
			$bookDate: String!
			$availedNumber: Int!
			
	) {
		createTransaction(
			memberId: $memberId
			bookDate: $bookDate
			availedNumber: $availedNumber
		) {
			memberId
			bookDate
			availedNumber
			
		}
	}
`;

const createTravelTourMutation = gql`
		mutation(
			$description:String!
			$price: String!
			$availability: Int!
			
	) {
		createTravelTour(
			description: $description
			price: $price
			availability: $availability
		) {
			description
			price
			availability
			
		}
	}
`;

const deleteTransactionMutation = gql`
	mutation($id: String!) {
		deleteTransaction(id: $id)
	}
`;


const deleteTravelTourMutation = gql`
	mutation($id: String!) {
		deleteTravelTour(id: $id)
	}
`;



const updateTravelTourMutation = gql`
	mutation($id: String! $description:String! $price:String! $availability:Int!) {
		updateTour(
		id: $id
		description: $description
		price: $price
		availability: $availability

		){
			id
			description
			price
			availability

		}

	}
`;

const updateTransactionMutation = gql`

mutation($id: String! $availedNumber: Int!)
 {
 		updateTransaction(
 			id:$id
 			availedNumber:$availedNumber
 		){
 		id
 		availedNumber
 		bookDate
 		memberId
	}
 }

`;

export {updateTransactionMutation,updateTravelTourMutation, deleteTravelTourMutation,deleteTransactionMutation,createMemberMutation,loginMemberMutation,createTransactionMutation,createTravelTourMutation};
