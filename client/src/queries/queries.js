import {gql} from "apollo-boost";


const getMemberQuery = gql`

	 {
		getMember {
			id
			firstName
			lastName
			email
			password
			country
			contact
		
	}
}

`;

const getTravelToursQuery = gql`
		{	
			getTravelTours
			{
				id
				description
				price
				availability
			}
	}
`;

const getTravelTourQuery = gql`
		{	
			getTravelTour 
			{
				id
				description
				price
				availability
			}
	}
`;


const getMemberByEmailQuery = gql`
    query($email: String!)
    {
        getMemberByEmail(email: $email){
          
          password
          email

          }
      }
`;


const getMemberAllQuery = gql`
        {
          getMember{
          	email
            firstName
            lastName
            }
        }
        `;



const getTransactionsQuery = gql`
{
	getTransactions
	{
		id
		memberId
		bookDate
		availedNumber
		
	}
}

`;

const getTransactionQuery = gql`

		query($memberId: String) {
		getTransaction (memberId: $memberId)
	{	
		memberId
		bookDate
		availedNumber
		id

	}
}

`;




export { getMemberQuery,getMemberAllQuery,getTravelToursQuery,getMemberByEmailQuery,getTransactionsQuery,getTravelTourQuery,getTransactionQuery };