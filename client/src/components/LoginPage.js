import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, Button} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";
import { Redirect } from "react-router-dom";



import HomePage from "../components/HomePage.js";

import {
  getMemberAllQuery, getMemberByEmailQuery
} from "../queries/queries";



import {
  loginMemberMutation
} from "../queries/mutations";


const divStyle = {
	  margin: '2px',
	  paddingTop:'10px'
	  
	};


const LoginPage = props => {




  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [loginToken, setLoginToken] = useState("");
  const [id, setMemberId] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setlastName] = useState("");
  
  const [loginSuccess, setLoginSuccess] = useState(false);


  const loginEmailHandler = (e) =>{

    console.log(e.target.value);
    setLoginEmail(e.target.value);
    console.log(loginEmail);
  }


  const loginPasswordHandler = (e) =>{
    console.log(e.target.value);
    setLoginPassword(e.target.value);
    console.log(loginPassword)
  }





  const loginUserHandler = e => {
    e.preventDefault();
    console.log(loginEmail);
    console.log(loginPassword);
    console.log(loginToken);
    console.log(id);
    console.log(firstName);
    console.log(lastName);

    props.loginMemberMutation({
      variables: {
        id: id,
        email: loginEmail,
        password: loginPassword,
        token: loginToken,
        firstName: firstName,
        lastName:lastName
      }
    }).then(res => {
      console.log("Logging in")
      console.log(res);
      console.log(res.data);

      let data = res.data.logInMember;
      if(data === null){

      } else {
        

        localStorage.setItem("email", data.email)
        localStorage.setItem("password", data.password)
        localStorage.setItem("token", data.token)
        localStorage.setItem("id", data.id)
        localStorage.setItem("firstName", data.firstName)
        localStorage.setItem("lastName", data.lastName)
        setLoginSuccess(true)
        window.location.href="/HomePage";
      }


    });


  };


  if(!loginSuccess) {
  console.log("something went wrong")
} else {
  return <Redirect to="/HomePage" />

}


		 return(
    <React.Fragment>
    <div className="ml-auto">
      <form className="form-inline form-control-sm">
        <div className="input-group">
          <input type="text" className="form-control form-control-sm mx-1" placeholder="Email" onChange={loginEmailHandler}/>
          <input type="password" className="form-control form-control-sm mx-1" placeholder="Password" aria-label="Password" onChange={loginPasswordHandler}/>
          <button type="submit" className="btn-info mx-1 px-4" onClick={loginUserHandler}>Log In</button>
        </div>
      </form>

    </div>
    </React.Fragment>
  )
};


export default compose(
  graphql(getMemberAllQuery, { name: "getMemberAllQuery" }),
  graphql(getMemberByEmailQuery, { name: "getMemberByEmailQuery" }),
  graphql(loginMemberMutation, { name: "loginMemberMutation" })
)(LoginPage);
// export default LoginModule;
