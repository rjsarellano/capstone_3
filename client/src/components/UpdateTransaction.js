
import Moment from 'react-moment';
import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, progress} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import { Modal } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

import { Link } from "react-router-dom";

import { Navbar } from "react-bulma-components";

import { Button,Tabs,Tab, Table } from 'react-bootstrap';
import {createTravelTourMutation,deleteTransactionMutation, updateTransactionMutation} from "../queries/mutations.js";

import {getTransactionQuery,getTransactionsQuery,getTravelTourQuery} from "../queries/queries.js"





const UpdateTransaction = props => {
console.log(props)


	const [transaction, setTransaction] = useState("");
	const [updateInput, setUpdateInput] = useState(false);
	const [availedNumber, setAvailedNumber] = useState(0);



	const transactionChangeHandler = e => {
		setUpdateInput(true);
		setTransaction(e.target.value);
	}

	const availedNumberChangeHandler = e => {
		setAvailedNumber(e.target.value)
	}



	



let data = props.getTransactionQuery.getTransaction ? props.getTransactionQuery.getTransaction : [];




				const transaction_list = e => {
									if (data) {
										return data.map((transaction, index) => {

											console.log(transaction.availedNumber);
											const deleteTransactionHandler = e => {
												console.log(transaction.availedNumber);
										console.log(transaction.id);


										let id = transaction.id;

										Swal.fire({
											title: "Are you sure you want to delete?",
											text: "You won't be able to revert this!",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#3085d6",
											cancelButtonColor: "#d33",
											confirmButtonText: "Yes, delete it!"
										}).then(result => {
											if (result.value) {
												props.deleteTransactionMutation({
													variables: { id: id },
													refetchQueries: [{ query: getTransactionQuery }]
												});
												Swal.fire("Deleted!", "Your Tour has been Updated.", "success");
												window.location.href="/UpdateTransaction";
											}
										});
									};	

									const updateTransactionHandler = e => {
										e.preventDefault();
										console.log(availedNumber);

										Swal.fire({
											title: "Are you sure you want to update?",
											text: "You won't be able to revert this!",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#3085d6",
											cancelButtonColor: "#d33",
											confirmButtonText: "Yes, Update it!"
										}).then(result => {
											if (result.value) {
												props.updateTransactionMutation({
													variables: {
												id: transaction.id,
												availedNumber: Number(availedNumber)},
													refetchQueries: [{ query: getTransactionQuery }]
												});
												Swal.fire("Updated!", "Your file has been deleted.", "success");
												window.location.href="/UpdateTransaction";
												
											}
										});
									};


							return (
								<tr key={index}>
									
									<td> {transaction.bookDate}</td>
									<td>{transaction.availedNumber}</td>
									<td> 

								<form onSubmit={updateTransactionHandler} id={transaction.id}>
									<input name="availed" type="number" defaultValue={availedNumber} onChange={availedNumberChangeHandler} />
								</form> 
									 </td>
										<td>  
											<Button variant="success" type="submit" form={transaction.id}> Confirm Edit </Button>
											<Button type="button" variant="danger" onClick={deleteTransactionHandler}>Cancel Booking </Button>
										</td>
									</tr>
							)
						})
					}
				}

	


// console.log(props.getTransactionQuery)
			return (
			<Container>
				<Columns>
					<Columns.Column >
						<Card>
							<Card.Header>
								<Card.Header.Title><p>Availed Transactions</p></Card.Header.Title>
							</Card.Header>
							<Card.Content>
							<Table striped bordered hover>
									  <thead>
									    <tr>
									      <th>Date Selected </th>
									      <th> Amount Availed </th>
									      <th> Edit Amount </th>
									      <th> Confirm and Pay </th>
									    </tr>
									  </thead>
									  <tbody>	

										{transaction_list()}

										
										
										
									  </tbody>
									</Table>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
			</Container>
		)
}


export default compose(


 graphql(getTransactionQuery, 
 	{ 
 		options: props => {
 			return {
 				variables: {
 					memberId: localStorage.id
 				}
 			}
 		}, 

 		name: "getTransactionQuery" }
 ),



 graphql(deleteTransactionMutation, { name: "deleteTransactionMutation" }),
  graphql(updateTransactionMutation, { name: "updateTransactionMutation" }),
  graphql(getTransactionsQuery, { name: "getTransactionsQuery" })

 ) (UpdateTransaction);