

import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, Button} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";
import { Redirect } from "react-router-dom";






const LogoutPage = props => {

	const loggedUser = props => <loginPage {...props} updateSession={updateSession} /> 
	 
	const [firstName, setFirstName] = useState(localStorage.getItem('firstName'))
	  const [token, setToken] = useState(localStorage.getItem('token'))
	  const [email, setEmail] = useState(localStorage.getItem('email'))


   const updateSession = () => {
    setFirstName(localStorage.getItem('firstName'))
    setToken(localStorage.getItem('token'))
  }

	 localStorage.clear()
    // updates and clears the values of our session hooks
    updateSession()
    //redirects to /login
    return <Redirect to='/loginPage'/>

  
};


export default LogoutPage;