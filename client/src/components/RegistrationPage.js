import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, Button} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";

import { getMemberQuery } from "../queries/queries.js";
import {createMemberMutation} from "../queries/mutations.js";


const divStyle = {
	  margin: '2px',
	  paddingTop:'10px'
	  
	};


const RegistrationPage = props => {

	console.log(props)


	//one way binding -> when the input can change the current state
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [country, setCountry] = useState("");
	const [password, setPassword] = useState("");
	const [contact, setContact] = useState("");

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
	}
	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
	}
	const emailChangeHandler = e => {
		setEmail(e.target.value);
	}
	const countryChangeHandler = e => {
		setCountry(e.target.value);
	}

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	}

	const contactChangeHandler = e => {
		setContact(e.target.value);
	}



		let data = props.getMemberQuery ? props.getMemberQuery.getMember : [];
	

	console.log(props.getMemberQuery);


	const submitFormHandler = (e) => {

		e.preventDefault();
		// console.log(e.target.firstName.value, firstName);
		let newMember = {
			firstName: firstName,
			lastName: lastName,
			country: country,
			password: password,
			email: email,
			contact: contact
		}

		setFirstName("");
		setLastName("");
		setCountry("");
		setPassword("");
		setEmail("");
		setContact("");

		

		console.log(newMember);

		
		 props.createMemberMutation({
		 	variables: newMember,
		 	refetchQueries: [
		 		{
		 			query: getMemberQuery
				}
		 	]
		 });
	};



		return(



		<Container breakpoint="fullhd">
			<Columns className="is-vcentered">
				<Columns.Column >
					<Card>
						<Card.Header>
							<Card.Header.Title><p>Register</p></Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={submitFormHandler}>


					<div className="column is-half is-offset-one-quarter">
							  <p className="control has-icons-left has-icons-right ">
							    <input className="input is-rounded" style={divStyle} type="email" placeholder="Email" onChange={emailChangeHandler} value={email}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-envelope"></i>
							    </span>
							    <span className="icon is-small is-right">
							      <i className="fas fa-check"></i>
							    </span>
							  </p>


							  <p className="control has-icons-left">
							    <input className="input is-rounded" style={divStyle} type="password" placeholder="Password" onChange={passwordChangeHandler} value={password}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>



							 <p className="control has-icons-left ">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="Firstname" onChange={firstNameChangeHandler} value={firstName}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>

							  

							  <p className="control has-icons-left">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="Lastname" onChange={lastNameChangeHandler} value={lastName}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>



							  <p className="control has-icons-left">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="Country" onChange={countryChangeHandler} value={country}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>


							  <p className="control has-icons-left">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="Contact" onChange={contactChangeHandler} value={contact}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>

							  	<Button type="submit" className="is-rounded is-primary"> Register </Button>

								</div>

					


							</form>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
			</Container>
		)
}



export default compose(

 graphql(createMemberMutation, {name: "createMemberMutation"}),
graphql(getMemberQuery, { name: "getMemberQuery" }))
(RegistrationPage);