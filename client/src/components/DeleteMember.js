import React, { useState } from "react";
import Swal from "sweetalert2";
import { Link, Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import "react-bulma-components/dist/react-bulma-components.min.css";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container
} from "react-bulma-components";

import { getMemberQuery, getTeamsQuery } from "../queries/queries";
import { updateMemberMutation } from "../queries/mutations";