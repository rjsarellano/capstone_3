import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Navbar } from "react-bulma-components";

const NavBar = props  => {



	console.log(props)
	const [open, setOpen] = useState(false);
	

	let memberToken = localStorage.getItem('token')
	let memberFirstName = localStorage.getItem('firstName')
	let memberId = localStorage.getItem('id')


console.log(memberId);

	const logoutHandler = e => {
	e.preventDefault();
	localStorage.clear();
	window.location.href="/"

}

	return (



		<Navbar color="black" active={open}  >


			<Navbar.Brand>
				<Navbar.Item to="/HomePage">
					<strong> Tour Booking System </strong>
				</Navbar.Item>
				<Navbar.Burger active={open.toString()} onClick={() => { setOpen(!open) }} />
			</Navbar.Brand>
			<Navbar.Menu active={open.toString()}>
				<Navbar.Container>
					<Link to="/HomePage" className="has-text-white-desktop navbar-item">Home Page</Link>
					<Link to="/AdminPage" className="has-text-white-desktop navbar-item">Admin</Link>
					<Link to="/RegistrationPage" className="has-text-white-desktop navbar-item">Register</Link>


{memberToken ? <Link to="/UpdateTransaction" className="has-text-white-desktop navbar-item">Transactions</Link>
:  
[]
}

{memberToken ? <Link to="/LoginPage" onClick={logoutHandler} className="has-text-white-desktop navbar-item">Logout  Welcome {memberFirstName}!</Link> 

:  
<Link to="/LoginPage" className="has-text-white-desktop navbar-item"> Login </Link>
}



  
	     			
						
				</Navbar.Container>
			</Navbar.Menu>
			
		</Navbar>
	)
}

export default NavBar;