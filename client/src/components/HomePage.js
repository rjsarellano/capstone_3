

import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, progress} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { Modal,Button } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';



import { Link } from "react-router-dom";
import { Navbar } from "react-bulma-components";

import {getTravelToursQuery,getTransactionQuery} from "../queries/queries.js";
import {createTransactionMutation} from "../queries/mutations.js";






const divStyle = {
	  
	  paddingTop:'20px',
	  paddingBottom: '20px'
	  
	};

const HomePage = props => {


		
		let data = props.getTravelToursQuery.getTravelTours
		? props.getTravelToursQuery.getTravelTours
		: [];

	console.log(props.getTravelToursQuery.getTravelTours);


	const [open, setOpen] = useState(false);
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [availability, setAvailability] = useState("");
	const [availNumber, setAvailNumber] = useState("");
	const [startDate, setStartDate] = useState(new Date());
	

	const availNumberChangeHandler = e => {
		setAvailNumber(e.target.value);
	}



/*Swal.fire({
			title: "Booking added",
			text: "Please confirm payment!",
			type: "success"
			}).then(respone => {
			window.location.reload()
			})
*/


 const [availabilityId, setAvailabilityId] = useState("");

  const availabilityIdHandler = () => {
	setAvailabilityId();

  }


   
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  
   
	

	

	const displayTravelTours = e => {
		e.preventDefault();
		let getTravelTours = {
			description: description,
			price: price,
			availability: availability
		};

		console.log(displayTravelTours);
	};


		let NewTransaction = {
		memberId: localStorage.getItem("id"),
		bookDate: startDate,
		availedNumber: parseInt(availNumber),
		firstName: localStorage.getItem("firstName"),
		lastName: localStorage.getItem("lastName")

}



	const displayTravelToursEvent = () => {
		let tourData = props.getTravelToursQuery;
		if (tourData.loading) {
			return <div>Loading... </div>;
		} else {
			return tourData.getTravelTours.map(tour => {
				console.log(tour)

					const submitTransactionHandler = e => {
							e.preventDefault();

							setShow(false);

									props.createTransactionMutation({
								 	variables: NewTransaction,
								 	refetchQueries: [
								 		{
								 			query: getTransactionQuery
										}
								 	]
								 });


							console.log(NewTransaction);
							console.log(tour.id);
							window.location.href="/HomePage";
							}







				return (
						<div className="column">
							   			<div className="card" >
									  <div className="card-image">
									    <figure className="image is-4by3">
									      <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image"/>
									    </figure>
									  </div>
									  <div className="card-content">
									    <div className="media">
									      <div className="media-left">
									        <figure className="image is-48x48">
									          <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image"/>
									        </figure>


									      </div>
									      <div className="media-content">
									        <p className="title is-4"> </p>
									        <p className="subtitle is-6">{tour.description}</p>


									      </div>
									    </div>

									    <div className="content">
									      
									      <br/>
									      <time dateTime="2016-1-1"> <DatePicker selected={startDate} onChange={date => setStartDate(date)} />   </time>

									      	<div> {tour.price} </div>

									      	<div> {tour.availability} Trips Available</div>
									    	</div>

									  		<form >   
											  	<div style={divStyle}>
												<Button className="button is-primary" onClick={setShow}> Book Now </Button>
												</div>
									    	</form>
									  </div>
									</div>



							<div>
					      		<Modal show={show} onHide={handleClose}>
					       				 <Modal.Header closeButton>
					        				  <Modal.Title>Modal heading</Modal.Title>
					        			</Modal.Header>
					        			<Modal.Body>

					        			<h4>Here is your transaction</h4>
					        			<p> {tour.description}  </p>
					        			<p> {tour.startDate}  </p>

					        			
					        			<input className="is-medium"  onChange={availNumberChangeHandler} type="number" min="1" max={tour.availability}  /> 

					        			</Modal.Body>
					      
					         	 <Button className="button is-success" onClick={handleClose}>
					            Close
					         	 </Button>
					         	 <Button className="button is-primary" onClick={submitTransactionHandler}>
					            Save Changes
					         	 </Button>

					     	 	</Modal>
							</div>
 						</div>

				);
			});
		}
	};




	return (
		
				<Container style={divStyle}>
					<div className="columns is-variable is-1-mobile is-0-tablet is-3-desktop is-8-widescreen is-2-fullhd">
							  {displayTravelToursEvent()}

					</div>
							
				</Container>
		
	)
}

export default compose(

graphql(createTransactionMutation, {name: "createTransactionMutation"}),
graphql(getTransactionQuery, {name: "getTransactionQuery"}),
graphql(getTravelToursQuery, {name : "getTravelToursQuery"}))(HomePage);