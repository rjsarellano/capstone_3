

import Moment from 'react-moment';
import React, {useState, useEffect} from "react";
import {graphql} from "react-apollo";
import {Container, Columns, Card, progress} from "react-bulma-components";
import {flowRight as compose} from "lodash";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { Modal } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

import { Link } from "react-router-dom";
import { Navbar } from "react-bulma-components";


import { Button,Tabs,Tab, Table } from 'react-bootstrap';
import {createTravelTourMutation,deleteTravelTourMutation,updateTravelTourMutation } from "../queries/mutations.js";
import {getTravelToursQuery} from "../queries/queries.js";
import {getTransactionsQuery} from "../queries/queries.js"



const divStyle = {
	  margin: '2px',
	  paddingTop:'10px'
	  
	};



const AdminPage = props => {


let travelTourData = props.getTravelToursQuery.getTravelTours ? props.getTravelToursQuery.getTravelTours: [];


	
console.log(travelTourData)


	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [availability, setAvailability] = useState("");
	const [removeTour, setRemoveTour] = useState('');


	const [updateDescription, setUpdateDescription] = useState("");
	const [updatePrice, setUpdatePrice] = useState("");
	const [updateAvailability, setUpdateAvailability] = useState("");




	 const [show, setShow] = useState(false);

	 const updateFormHandler = e => {
	 	setShow(true)

	 	console.log(e.target.getAttribute("data-description"));
	 	setUpdateDescription(e.target.getAttribute("data-description"))
	 	setUpdatePrice(e.target.getAttribute("data-price"))
	 	setUpdateAvailability(e.target.getAttribute("data-availability"))
	 }


  	const handleClose = () => setShow(false);
 	 const handleShow = () => setShow(true);




	const descriptionChangeHandler = e => {
		e.preventDefault()
		setDescription(e.target.value);
	}
	const priceChangeHandler = e => {
		e.preventDefault()
		setPrice(e.target.value);
	}
	const availabilityChangeHandler = e => {
		e.preventDefault()
		setAvailability(e.target.value);
	}






/*********************UPDATE HANDLERS********************************************/
	const updateDescriptionChangeHandler = e => {
		setUpdateDescription(e.target.value);
	};

	


	const updatePriceChangeHandler = e => {
		setUpdatePrice(e.target.value);
	};



	const updateAvailabilityChangeHandler = e => {
		setUpdateAvailability(e.target.value);
	};



		const submitFormHandler = (e) => {

		e.preventDefault();
		// console.log(e.target.firstName.value, firstName);
		let newTravelTour = {
			description: description,
			price: price,
			availability: parseInt(availability),
			
		}

		setDescription("");
		setPrice("");
		setAvailability("");
		
		
		 props.createTravelTourMutation({
		 	variables: newTravelTour,
		 	refetchQueries: [
		 		{
		 			query: getTravelToursQuery
				}
		 	]
		 });
	};




							const travelTour_list = e => {
								
									if (travelTourData) {
										return travelTourData.map((travelTour, index) => {

											const deleteTransactionHandler = e => {
												e.preventDefault()
										console.log(travelTour);
										let id = travelTour.id;
										Swal.fire({
											title: "Are you sure you want to delete?",
											text: "You won't be able to revert this!",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#3085d6",
											cancelButtonColor: "#d33",
											confirmButtonText: "Yes, delete it!"
										}).then(result => {
											if (result.value) {
												props.deleteTravelTourMutation({
													variables: { id: id },
													refetchQueries: [{ query: getTravelToursQuery }]
												});
												Swal.fire("Deleted!", "Your file has been deleted.", "success");
												
											}
										});
									};	

									const updateSubmitHandler = e => {
										e.preventDefault()
										console.log(updateDescription, updatePrice, updateAvailability)
										
										Swal.fire({
											title: "Are you sure you want to update?",
											text: "You won't be able to revert this!",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#3085d6",
											cancelButtonColor: "#d33",
											confirmButtonText: "Yes, update it it!"
										}).then(result2 => {
											if (result2.value) {
												props.updateTravelTourMutation({
													variables: { id: travelTour.id,
																 description: updateDescription,
																price: updatePrice,
																availability: Number(updateAvailability)
													},
													refetchQueries: [{ query: getTravelToursQuery }]
												});
												Swal.fire("Updated!", "Your file has been updated.", "success");
												handleClose();

											}
										});
									};	


							return (


								/*****************************Remove Tour **************************/
								<tr key={index}>
									<td> {travelTour.description}</td>
									<td>{travelTour.price}</td>
									<td>{travelTour.availability}</td>
									
									<td>  
										<Button 

											data-description={travelTour.description}
											data-price={travelTour.price}
											data-availability={travelTour.availability}
											type="button"
											onClick={updateFormHandler}
										>  
											Edit Content
										</Button>

						<Button variant="danger" onClick={deleteTransactionHandler}>Remove Tour</Button>


										

								{/****************************Upate Modal with Form*********************************/}
						<div>
					      	<Modal show={show} onHide={handleClose} centered>
							<form onSubmit={updateSubmitHandler} >

					       		<Modal.Header closeButton>
					        				  <Modal.Title>Modal heading</Modal.Title>
					        			</Modal.Header>
					        			<Modal.Body>
					        				
					        					<div> description<input type="text" name="description" defaultValue={updateDescription} onChange={updateDescriptionChangeHandler}/> </div>
					        					<div> Price<input type="text" name="price" defaultValue={updatePrice} onChange={updatePriceChangeHandler}/> </div>
					        					<div>availability<input type="number" name="availability" defaultValue={updateAvailability} onChange={updateAvailabilityChangeHandler}/></div>
															        			</Modal.Body>
							         	 <Button type="button" className="button is-success" onClick={handleClose}  >
							            Close
							         	 </Button>
							         	 <Button type="submit" className="button is-primary" >
							            Save Changes
							         	</Button>
							 </form>

							  </Modal>

						</div>
				</td>
			</tr>





							)

						})
					}
				}






// **********************************ADDING TOUR ***************************************

		return(
		<Container breakpoint="fullhd" >
			<Columns className="is-vcentered">
				<Columns.Column >
					<Card>
						<Card.Header>
							<Card.Header.Title><p>Add Tour</p></Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={submitFormHandler}>


					<div className="column is-half is-offset-one-quarter">
							 
							  <p className="control has-icons-left has-icons-right ">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="description" onChange={descriptionChangeHandler} value={description}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-envelope"></i>
							    </span>
							    <span className="icon is-small is-right">
							      <i className="fas fa-check"></i>
							    </span>
							  </p>


							  <p className="control has-icons-left">
							    <input className="input is-rounded" style={divStyle} type="text" placeholder="price" onChange={priceChangeHandler} value={price}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>



							 <p className="control has-icons-left ">
							    <input className="input is-rounded" style={divStyle} type="number" placeholder="availability" onChange={availabilityChangeHandler} value={availability}/>
							    <span className="icon is-small is-left">
							      <i className="fas fa-lock"></i>
							    </span>
							  </p>

							 
							  	<Button type="submit" className="is-rounded is-primary"> Add Tour </Button>

								</div>

					
							</form>
								</Card.Content>
							</Card>
						</Columns.Column>

{/*///////////////////////////////////////////////////////////////////////////////////*/}

					<Columns.Column >
					<Card>
						<Card.Header>
							<Card.Header.Title><p>Delete and Update Tours</p></Card.Header.Title>
						</Card.Header>
						<Card.Content>

									<Table>
									<thead>
									    <tr>
									      <th>Tour Name</th>
									      <th>Tour Price</th>
									      <th>availability</th>
									      <th>Action</th>
									    </tr>
									  </thead>
									  <tbody>	

									  	{travelTour_list()}
						
									  </tbody>
									</Table>


								</Card.Content>
							</Card>
						</Columns.Column>
							


						
					</Columns>
			</Container>

	  )
};



export default compose(

 graphql(createTravelTourMutation, {name: "createTravelTourMutation"}),
 graphql(updateTravelTourMutation, {name: "updateTravelTourMutation"}),
  graphql(deleteTravelTourMutation, {name: "deleteTravelTourMutation"}),
 graphql(getTransactionsQuery, { name: "getTransactionsQuery" }),
graphql(getTravelToursQuery, { name: "getTravelToursQuery" }))
(AdminPage);